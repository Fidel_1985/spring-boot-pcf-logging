package hello;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    private static final int maxId = 3;
    
    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name)
      throws Exception {
        //Info as we don't need to capture this level of details all the time
        log.info("Greeting controller called with name: {}", name);
        try {
          long currentId = counter.incrementAndGet();
          if (currentId > maxId) {
            //Simulating error to generate an error log entry
            throw new Exception(String.format("Max id value of %s has been exceeded with the current value of %s", maxId, currentId));
          } else {
            return new Greeting(currentId,
                                String.format(template, name));
          }
        } catch (Exception ex) {
          //Error level to capture the details of the exception
          log.error(ex.toString());
          throw ex;
        }
    }
}
